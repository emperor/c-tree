CC = gcc
CFLAGS = -Wall

MODULES_SRCS = tree.c \
               tree_formatter.c

MODULES_OBJS = $(MODULES_SRCS:.c=.o)

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

all: $(MODULES_OBJS)
	$(CC) $(CFLAGS) -o tree main.c $(MODULES_OBJS)

rebuild_all: clean all

clean: clean_test
	rm -f tree
	rm -f *.o


TEST_PATH = test

MODULES_TEST_PATH = $(TEST_PATH)/test/modules

TESTS_SRCS = $(wildcard test/modules/*.c)

TESTS_OBJS = $(TESTS_SRCS:.c=.o)

test: $(MODULES_OBJS) $(TESTS_OBJS) test_suite
	@echo $(TESTS_OBJS)
	$(CC) $(CFLAGS) -I./ -o test/tree_test test/main.c $(TEST_PATH)/test_suite.o $(TESTS_OBJS) $(MODULES_OBJS)
	./test/tree_test

test_suite:
	$(CC) $(CFLAGS) -c $(TEST_PATH)/test_suite.c -o $(TEST_PATH)/test_suite.o


clean_test:
	rm -f test/tree_test	
	rm -f test/*.o
	rm -f test/modules/*.o
