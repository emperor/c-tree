#include <stddef.h>
#include <stdio.h>

#include "tree.h"
#include "tree_formatter.h"

void print_node_pointers(struct node *node) {
    void *left = node->left;
    void *right = node->right;
    printf("left is at %p, right is at %p\n", left, right);
}
