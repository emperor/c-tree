#include <stddef.h>

#include "tree.h"

void swap(struct node *tree) {
    void *tmp_pointer = NULL;
    tmp_pointer = tree->left;
    tree->left = tree->right;
    tree->right = tmp_pointer;
}
