#include <stdio.h>
#include "tree.h"
#include "tree_formatter.h"

int main() {
    printf("sample tree management program\n");

    struct node tree = {NULL, NULL};

    print_node_pointers(&tree);

    return 0;
}
