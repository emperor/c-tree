#ifndef TREE_H
#define TREE_H


/* tree struct */

struct leaf {
    int number;
};

struct node {
    void *left;
    void *right;
};


/* tree operation functions */

void swap(struct node*);


#endif
