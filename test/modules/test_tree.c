#include <stdio.h>

#include "../../tree.h"
#include "test_tree.h"
#include "../test_suite.h"

int test_swap() {
    struct leaf left = {3};
    struct leaf right = {4};
    struct node tree = {&left, &right};

    swap(&tree);

    struct leaf *new_left = tree.left;
    struct leaf *new_right = tree.right;

    assert_true(new_left->number == 4, "swap: left value is wrong");
    assert_true(new_right->number == 3, "swap: right value is wrong");

    return 0;
}

int test_tree() {
    printf("Main tree module tests\n");

    test_swap();

    return 0;
}
