#include <stdio.h>

#include "test_suite.h"
#include "modules/test_tree.h"

int main() {
    printf("Running tree program tests...\n");

    test_tree();

    print_stats();

    return 0;
}
