#include <stdio.h>
#include <string.h>

#include "test_suite.h"

#define true 1
#define false 0


struct {
    int assert_count;
    int succeed_count;
    int failed_count;
} test_stats;


int assert_true(int result, char *message) {
    test_stats.assert_count++;
    
    if (result) {
        printf(".");
        test_stats.succeed_count++;
    } else {
        test_stats.failed_count++;
        printf("Assertion failed: %s\n", message);
    }

    return true;
}

void print_stats() {
    printf("\n%d assertions, %d succeed, %d failed\n", test_stats.assert_count, test_stats.succeed_count, test_stats.failed_count);
}
